package com.penzhulyak.basejava.diffutills;

import androidx.recyclerview.widget.DiffUtil;

import com.penzhulyak.basejava.data.local.db.entity.User;

import java.util.List;

public class UserListDiffUtil extends DiffUtil.Callback {

    private List<User> mOldList, mNewList;

    public UserListDiffUtil(List<User> oldList, List<User> newList) {
        mOldList = oldList;
        mNewList = newList;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldList.get(oldItemPosition).getId() == (mNewList.get(newItemPosition).getId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        boolean equalsLogin = mOldList.get(oldItemPosition).getLogin().equals(mNewList.get(newItemPosition).getLogin());
        return equalsLogin;
    }
}
