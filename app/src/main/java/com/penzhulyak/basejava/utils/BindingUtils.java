package com.penzhulyak.basejava.utils;

import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import static android.view.View.VISIBLE;

public class BindingUtils {

    public BindingUtils() {

    }

    @BindingAdapter("app:visibility")
    public static void onVisibility(View view, boolean visibility) {
        view.setVisibility(visibility ? VISIBLE : View.GONE);
    }

    @BindingAdapter("bind:textHtml")
    public static void onTextHtmlShow(TextView view, String textHtml) {
        view.setText(Html.fromHtml(textHtml));
    }

    @BindingAdapter("bind:animVisibility")
    public static void animateView(View view, boolean animVisibility) {
        if (animVisibility) {
            TranslateAnimation animate = new TranslateAnimation(0, 0, -view.getHeight(), 0);
            animate.setDuration(200);
            animate.setFillAfter(true);
            view.startAnimation(animate);
            view.setVisibility(VISIBLE);
        } else {
            TranslateAnimation animate = new TranslateAnimation(0, 0, 0, -view.getHeight());
            animate.setDuration(200);
            animate.setFillAfter(true);
            animate.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    view.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            view.startAnimation(animate);
        }
    }
}
