package com.penzhulyak.basejava.interactors;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class SearchInteractor {
    private final MutableLiveData<String> searchField = new MutableLiveData<>();

    @Inject
    public SearchInteractor() {
    }

    public MutableLiveData<String> getSearchField() {
        return searchField;
    }

    public void setSearchField(String value) {
        searchField.setValue(value);
    }
}
