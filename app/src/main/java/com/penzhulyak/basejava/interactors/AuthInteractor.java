package com.penzhulyak.basejava.interactors;


import androidx.lifecycle.MutableLiveData;

import com.penzhulyak.basejava.data.AppRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AuthInteractor {
    private final MutableLiveData<Boolean> auth = new MutableLiveData<>();
    private final MutableLiveData<Integer> lockType = new MutableLiveData<>();
    private final MutableLiveData<Long> lastActivities = new MutableLiveData<>();
    private final MutableLiveData<Boolean> isBiometricLocked = new MutableLiveData<>();
    private final MutableLiveData<Integer> biometricLockTimer = new MutableLiveData<>();
    private AppRepository mRepository;

    @Inject
    public AuthInteractor(AppRepository repository) {
        mRepository = repository;
        auth.setValue(mRepository.getAuthorization());
        lockType.setValue(mRepository.getLockType());
        lastActivities.setValue(mRepository.getLastActivities());
    }

    public void setAuth(boolean value) {
        auth.setValue(value);
        mRepository.setAuthorization(value);
    }

    public MutableLiveData<Long> getLastActivities() {
        return lastActivities;
    }

    public void setLastActivities(long time) {
        lastActivities.setValue(time);
        mRepository.setLastActivities(time);
    }

    public MutableLiveData<Boolean> isAuth() {
        return auth;
    }


    public MutableLiveData<Integer> getLockType() {
        return lockType;
    }

    public void setLockType(Integer type) {
        lockType.setValue(type);
        mRepository.setLockType(type);
    }

    public MutableLiveData<Boolean> isBiomtricLocked() {
        return isBiometricLocked;
    }

    public void setBiomtricLocked(boolean value) {
        isBiometricLocked.setValue(value);
    }

    public MutableLiveData<Integer> getBiomtricLockTimer() {
        return biometricLockTimer;
    }

    public void setBiometricLockTimer(int value) {
        biometricLockTimer.setValue(value);
    }
}
