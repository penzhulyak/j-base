package com.penzhulyak.basejava.data.local.db;

import androidx.lifecycle.LiveData;

import com.penzhulyak.basejava.data.local.db.entity.User;

import java.util.List;

public interface IDatabaseHelper {
    void insertUser(User user);

    LiveData<List<User>> getAllUsers();

    LiveData<List<User>> getFilteredUsers(String login);

    void isUserValid(String login, String password, ValidationUserCallback callback);

    interface ValidationUserCallback {
        void onUserIsValid();

        void onUserIsInvalid();
    }
}
