package com.penzhulyak.basejava.data;


import com.penzhulyak.basejava.data.local.db.IDatabaseHelper;
import com.penzhulyak.basejava.data.local.prefs.IPreferencesHelper;

public interface IDataSourceManager extends IPreferencesHelper, IDatabaseHelper {

}
