package com.penzhulyak.basejava.data;

import android.util.Log;

import androidx.lifecycle.LiveData;

import com.penzhulyak.basejava.data.local.db.DataBaseHelper;
import com.penzhulyak.basejava.data.local.db.entity.User;
import com.penzhulyak.basejava.data.local.prefs.PreferencesHelper;

import java.util.List;

import javax.inject.Inject;

public class AppRepository implements IDataSourceManager {
    private final PreferencesHelper mPreferencesHelper;
    private final DataBaseHelper mDatabaseHelper;

    @Inject
    AppRepository(PreferencesHelper preferencesHelper, DataBaseHelper databaseHelper) {
        mPreferencesHelper = preferencesHelper;
        mDatabaseHelper = databaseHelper;
    }

    @Override
    public void insertUser(User user) {
        mDatabaseHelper.insertUser(user);
    }

    @Override
    public LiveData<List<User>> getAllUsers() {
        Log.d("TAG_ALL", "empty");
        return mDatabaseHelper.getAllUsers();
    }

    @Override
    public LiveData<List<User>> getFilteredUsers(String login) {
        Log.d("TAG_FILTERED", login);
        return mDatabaseHelper.getFilteredUsers(login);
    }

    @Override
    public void isUserValid(String login, String password, ValidationUserCallback callback) {

    }

    @Override
    public void cleanPreferences() {
        mPreferencesHelper.cleanPreferences();
    }

    @Override
    public String getUser() {
        return null;
    }

    @Override
    public void setUser(String user) {

    }

    @Override
    public boolean getAuthorization() {
        return false;
    }

    @Override
    public void setAuthorization(boolean isAuth) {

    }

    @Override
    public String getLockScreenTimeOut() {
        return null;
    }

    @Override
    public void setLockScreenTimeOut(String timeOut) {

    }

    @Override
    public int getLockType() {
        return 0;
    }

    @Override
    public void setLockType(int type) {

    }

    @Override
    public String getLockCode() {
        return null;
    }

    @Override
    public void setLockCode(String code) {

    }

    @Override
    public long getLastActivities() {
        return 0;
    }

    @Override
    public void setLastActivities(long time) {

    }
}
