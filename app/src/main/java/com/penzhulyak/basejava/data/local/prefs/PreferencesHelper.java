package com.penzhulyak.basejava.data.local.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

public class PreferencesHelper implements IPreferencesHelper {

    private static final String PREF_KEY_AUTHORIZATION = "PREF_KEY_AUTHORIZATION";
    private static final String PREF_KEY_LOCK_SCREEN_TIME_OUT = "PREF_KEY_LOCK_SCREEN_TIME_OUT";
    private static final String PREF_KEY_LOCK_TYPE = "PREF_KEY_LOCK_TYPE";
    private static final String PREF_KEY_LOCK_CODE = "PREF_KEY_LOCK_CODE";
    private static final String PREF_KEY_USER = "PREF_KEY_USER";
    private static final String PREF_KEY_LAST_ACTIVITIES = "PREF_KEY_LAST_ACTIVITIES";

    private final SharedPreferences mPrefs;

    @Inject
    PreferencesHelper(Context context, String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public void cleanPreferences() {
        mPrefs.edit().clear().apply();
    }

    @Override
    public String getUser() {
        return mPrefs.getString(PREF_KEY_USER, null);
    }

    @Override
    public void setUser(String user) {
        mPrefs.edit().putString(PREF_KEY_USER, user).apply();
    }

    @Override
    public boolean getAuthorization() {
        return mPrefs.getBoolean(PREF_KEY_AUTHORIZATION, true);
    }

    @Override
    public void setAuthorization(boolean isAuth) {
        mPrefs.edit().putBoolean(PREF_KEY_AUTHORIZATION, isAuth).apply();
    }

    @Override
    public String getLockScreenTimeOut() {
        return mPrefs.getString(PREF_KEY_LOCK_SCREEN_TIME_OUT, "1000");
    }

    @Override
    public void setLockScreenTimeOut(String timeOut) {
        mPrefs.edit().putString(PREF_KEY_LOCK_SCREEN_TIME_OUT, timeOut).apply();
    }

    @Override
    public int getLockType() {
        return mPrefs.getInt(PREF_KEY_LOCK_TYPE, -1);
    }

    @Override
    public void setLockType(int type) {
        mPrefs.edit().putInt(PREF_KEY_LOCK_TYPE, type).apply();
    }

    @Override
    public String getLockCode() {
        return mPrefs.getString(PREF_KEY_LOCK_CODE, null);
    }

    @Override
    public void setLockCode(String code) {
        mPrefs.edit().putString(PREF_KEY_LOCK_CODE, code).apply();
    }

    @Override
    public long getLastActivities() {
        return mPrefs.getLong(PREF_KEY_LAST_ACTIVITIES, -1);
    }

    @Override
    public void setLastActivities(long time) {
        mPrefs.edit().putLong(PREF_KEY_LAST_ACTIVITIES, time).apply();
    }
}
