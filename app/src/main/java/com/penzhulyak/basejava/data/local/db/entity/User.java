package com.penzhulyak.basejava.data.local.db.entity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "users")
public final class User {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    @Nullable
    @ColumnInfo(name = "login")
    private String mLogin;

    @Nullable
    @ColumnInfo(name = "password")
    private String mPassword;

    private User(@NonNull int id, @Nullable String login, @Nullable String password) {
        mId = id;
        mLogin = login;
        mPassword = password;
    }

    public User(@Nullable String login, @Nullable String password) {
        mLogin = login;
        mPassword = password;
    }

    @NonNull
    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    @Nullable
    public String getLogin() {
        return mLogin;
    }

    public void setLogin(@Nullable String mLogin) {
        this.mLogin = mLogin;
    }

    @Nullable
    public String getPassword() {
        return mPassword;
    }

    public void setPassword(@Nullable String mPassword) {
        this.mPassword = mPassword;
    }

    @Override
    public String toString() {
        return "User :" +
                "mId='" + mId + '\'' +
                ", mLogin='" + mLogin + '\'' +
                ", mPassword='" + mPassword + '\'';
    }
}
