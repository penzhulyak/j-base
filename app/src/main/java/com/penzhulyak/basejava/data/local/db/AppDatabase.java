package com.penzhulyak.basejava.data.local.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.penzhulyak.basejava.data.local.db.dao.UserDao;
import com.penzhulyak.basejava.data.local.db.entity.User;

@Database(entities = {User.class}, version = AppDatabase.DB_VERSION, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    static final int DB_VERSION = 1;

    public abstract UserDao userDao();
}
