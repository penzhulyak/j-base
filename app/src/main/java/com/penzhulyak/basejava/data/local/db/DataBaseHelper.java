package com.penzhulyak.basejava.data.local.db;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import com.penzhulyak.basejava.data.local.db.dao.UserDao;
import com.penzhulyak.basejava.data.local.db.entity.User;
import com.penzhulyak.basejava.utils.AppExecutors;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DataBaseHelper implements IDatabaseHelper {

    private final AppDatabase mAppDatabase;
    private final AppExecutors mAppExecutors;
    private UserDao userDao;

    @Inject
    public DataBaseHelper(AppDatabase appDatabase, AppExecutors appExecutors) {
        mAppDatabase = appDatabase;
        mAppExecutors = appExecutors;
        userDao = mAppDatabase.userDao();
    }

    @Override
    public LiveData<List<User>> getAllUsers() {
        return userDao.getAllUsers();
    }

    @Override
    public LiveData<List<User>> getFilteredUsers(String login) {
        return userDao.getFilteredUsers(login);
    }

    @Override
    public void insertUser(@NonNull final User user) {
        Runnable saveRunnable = () -> userDao.insertUser(user);
        mAppExecutors.diskIO().execute(saveRunnable);
    }

    @Override
    public void isUserValid(String login, String password, ValidationUserCallback callback) {
        Runnable runnable = () -> {
            final List<User> users = userDao.getUsers();
            mAppExecutors.mainThread().execute(() -> {
                for (User user : users) {
                    if (user.getLogin().equals(login) && user.getPassword().equals(password))
                        callback.onUserIsValid();
                    else
                        callback.onUserIsInvalid();
                }
            });
        };
        mAppExecutors.diskIO().execute(runnable);
    }
}
