package com.penzhulyak.basejava.data.local.prefs;

public interface IPreferencesHelper {

    void cleanPreferences();

    String getUser();

    void setUser(String user);

    boolean getAuthorization();

    void setAuthorization(boolean isAuth);

    String getLockScreenTimeOut();

    void setLockScreenTimeOut(String timeOut);

    int getLockType();

    void setLockType(int type);

    String getLockCode();

    void setLockCode(String code);

    long getLastActivities();

    void setLastActivities(long time);
}
