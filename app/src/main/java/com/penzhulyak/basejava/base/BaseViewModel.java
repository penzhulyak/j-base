package com.penzhulyak.basejava.base;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.penzhulyak.basejava.interactors.AuthInteractor;
import com.penzhulyak.basejava.interactors.RequestsInteractor;
import com.penzhulyak.basejava.interactors.SearchInteractor;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class BaseViewModel extends ViewModel implements LifecycleOwner {
    public static final int NO_LOCK_TYPE = -1;
    public static final int GRAPHIC_LOCK_TYPE = 0;
    public static final int PIN_LOCK_TYPE = 1;
    public static final int BIOMETRIC_LOCK_TYPE = 2;
    private final MutableLiveData<String> showMessageToast = new MutableLiveData<>();
    protected CompositeDisposable disposable = new CompositeDisposable();
    @Inject
    AuthInteractor authInteractor;
    @Inject
    RequestsInteractor requestsInteractor;
    @Inject
    SearchInteractor searchInteractor;
    private LifecycleRegistry lifecycle = new LifecycleRegistry(this);

    public BaseViewModel(SearchInteractor searchInteractor) {
        this.searchInteractor = searchInteractor;
    }

//    public void setAuth(boolean auth) {
//        authInteractor.setAuth(auth);
//    }
//
//    public MutableLiveData<Boolean> isAuth() {
//        return authInteractor.isAuth();
//    }
//
//    public MutableLiveData<Long> getLastActivities() {
//        return authInteractor.getLastActivities();
//    }
//
//    public void setLastActivities(long time) {
//        authInteractor.setLastActivities(time);
//    }
//
//    public MutableLiveData<Integer> getLockType() {
//        return authInteractor.getLockType();
//    }
//
//    public void setLockType(Integer type) {
//        authInteractor.setLockType(type);
//    }
//
//    public MutableLiveData<Boolean> isBiometricLocked() {
//        return authInteractor.isBiomtricLocked();
//    }
//
//    public void setBiometricLocked(boolean value) {
//        authInteractor.setBiomtricLocked(value);
//    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return lifecycle;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        lifecycle.markState(Lifecycle.State.DESTROYED);
        disposable.clear();
    }

    public MutableLiveData<String> getShowMessageToast() {
        return showMessageToast;
    }

    public void setShowMessageToast(String message) {
        showMessageToast.setValue(message);
    }

    public MutableLiveData<Boolean> isProgressing() {
        return requestsInteractor.isProgressing();
    }

    public void setProgressing(boolean value) {
        requestsInteractor.setProgressing(value);
    }

    public MutableLiveData<String> getSearchField() {
        return searchInteractor.getSearchField();
    }

    public void setSearchField(String value) {
        if (searchInteractor != null)
            searchInteractor.setSearchField(value);
    }
}
