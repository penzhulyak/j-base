package com.penzhulyak.basejava.base;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.penzhulyak.basejava.R;
import com.penzhulyak.basejava.ui.main.IDrawerLocker;

import dagger.android.support.DaggerFragment;

public class BaseFragment extends DaggerFragment {
    ActionBar actionbar;
    Toolbar toolbar;

    protected void setBackArrowActionbar(boolean isShow, String title) {
        actionbar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (isShow) {
            actionbar.setDisplayHomeAsUpEnabled(true);
        } else
            actionbar.setDisplayHomeAsUpEnabled(false);
        actionbar.setDisplayShowHomeEnabled(true);
        actionbar.setTitle(title);
    }

    public Toolbar getToolbar() {
        return toolbar != null ? toolbar : (Toolbar) getActivity().findViewById(R.id.toolbar);
    }

    protected void setToolbarVisibility(boolean isVisible) {
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    protected void setDrawerEnabled(boolean value) {
        ((IDrawerLocker) getActivity()).setDrawerEnabled(value);
    }
}
