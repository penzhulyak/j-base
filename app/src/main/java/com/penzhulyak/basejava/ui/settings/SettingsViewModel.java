package com.penzhulyak.basejava.ui.settings;

import android.os.Handler;

import com.penzhulyak.basejava.base.BaseViewModel;
import com.penzhulyak.basejava.data.AppRepository;
import com.penzhulyak.basejava.interactors.SearchInteractor;

import javax.inject.Inject;

public class SettingsViewModel extends BaseViewModel {
    private AppRepository mRepository;

    @Inject
    public SettingsViewModel(AppRepository mAppRepository, SearchInteractor mSearchInteractor) {
        super(mSearchInteractor);
        mRepository = mAppRepository;
    }

    void init() {
        setProgressing(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setProgressing(false);
            }
        }, 5000);
    }
}