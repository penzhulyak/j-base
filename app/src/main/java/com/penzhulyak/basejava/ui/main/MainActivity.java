package com.penzhulyak.basejava.ui.main;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.transition.Fade;
import androidx.transition.TransitionManager;
import androidx.transition.TransitionSet;

import com.google.android.material.navigation.NavigationView;
import com.penzhulyak.basejava.BuildConfig;
import com.penzhulyak.basejava.R;
import com.penzhulyak.basejava.base.BaseActivity;
import com.penzhulyak.basejava.databinding.ActivityMainDrawerBinding;
import com.penzhulyak.basejava.di.viewmodel.ViewModelFactory;
import com.penzhulyak.basejava.utils.KeyboardUtil;

import javax.inject.Inject;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, NavigationView.OnClickListener, IActionBarBuilder {
    @Inject
    ViewModelFactory viewModelFactory;
    private ActivityMainDrawerBinding binding;
    private MainViewModel mViewModel;
    private ActionBarDrawerToggle toggle;
    private NavController navController;
    private NavigationView mNavView;
    private DrawerLayout mDrawer;
    private Toolbar mToolbar;
    private MenuItem searchItem;
    private SearchView searchViewAndroidActionBar;


    //toolbar customization
    private boolean isShowCheckIcon = false;
    private boolean isShowSearchIcon = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main_drawer);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel.class);
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mNavView = (NavigationView) findViewById(R.id.navView);

        setSupportActionBar(mToolbar);

        mViewModel.getOnSearchItemClicked().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                Log.d("TAG search", aBoolean + " clicked");
                if (aBoolean)
                    onSearchItemClick();
            }
        });

        toggle = new ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        mNavView.setNavigationItemSelectedListener(this);
//        NavigationUI.setupActionBarWithNavController(this, navController, mDrawer);
        toggle.syncState();

        updateHeader();
    }

    private void updateHeader() {
        View headerView = mNavView.getHeaderView(0);
        TextView version = headerView.findViewById(R.id.appVersion);
        version.setText("Version: " + BuildConfig.VERSION_NAME);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (!menuItem.isChecked()) {
            menuItem.setChecked(true);
            switch (id) {
                case R.id.action_global_categories_navigation:
                    navController.navigate(R.id.action_global_categories_navigation);
                    break;
                case R.id.action_global_settings_navigation:
                    navController.navigate(R.id.action_global_settings_navigation);
                    break;
                case R.id.action_global_custom_toolbar_navigation:
                    navController.navigate(R.id.action_global_custom_toolbar_navigation);
                    break;
            }
        }
        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        int id = navController.getCurrentDestination().getId();
        CharSequence label = navController.getCurrentDestination().getLabel();
        Log.d("TAG", id + "");
        Log.d("TAG", label + "");
        switch (id) {
            case R.id.categoriesFragment:
            case R.id.settingsFragment:
            case R.id.customToolbarFragment:
                finish();
                break;
            default:
                navController.popBackStack();
                break;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.item_check).setVisible(isShowCheckIcon);
        searchItem = menu.findItem(R.id.item_search);
        searchItem.setVisible(isShowSearchIcon);
        searchViewAndroidActionBar = (SearchView) searchItem.getActionView();
        searchViewAndroidActionBar.setQueryHint("Введите название заметки");
        LinearLayout searchEditFrame = (LinearLayout) searchViewAndroidActionBar.findViewById(R.id.search_bar);
        ((LinearLayoutCompat.LayoutParams) searchEditFrame.getLayoutParams()).leftMargin = 0;
        ((LinearLayoutCompat.LayoutParams) searchEditFrame.getLayoutParams()).rightMargin = -50;
        LinearLayout searchf = (LinearLayout) searchViewAndroidActionBar.findViewById(R.id.search_edit_frame);
        ((LinearLayout.LayoutParams) searchf.getLayoutParams()).leftMargin = 0;
//        ((LinearLayout.LayoutParams) searchf.getLayoutParams()).rightMargin = -50;

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        KeyboardUtil.hideKeyboard(this);
        switch (itemId) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.item_search:
                mViewModel.setOnSearchItemClicked(true);
                break;
            case R.id.item_check:
                Toast.makeText(this, "sync clicked", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onSearchItemClick() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                TransitionSet set = new TransitionSet()
//                        .addTransition(new Scale(0.7f))
                        .addTransition(new Fade())
                        .setInterpolator(new LinearOutSlowInInterpolator());
                TransitionManager.beginDelayedTransition((ViewGroup) findViewById(R.id.toolbar), set);
                searchItem.expandActionView();
                searchViewAndroidActionBar.setQuery(mViewModel.getSearchField().getValue(), false);
                searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        Log.d("TAG", "clear focus");
                        searchViewAndroidActionBar.clearFocus();
                        mViewModel.setOnSearchItemClicked(false);
                        return true;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        Log.d("TAG", "query changed " + newText);
                        mViewModel.setSearchField(newText);
                        return true;
                    }
                });
                searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionExpand(MenuItem menuItem) {
                        Log.d("TAG", "open");
                        return true;
                    }

                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                        Log.d("TAG", "close");
                        mViewModel.setOnSearchItemClicked(false);
                        return true;
                    }
                });
            }
        }, 500);
    }

    @Override
    public void setToolbarTitle(CharSequence title) {
        mToolbar.setTitle(title);
    }

    @Override
    public void setToolbarVisible(boolean visible) {

    }

    @Override
    public void showCheckItem(boolean showCheck) {
        Log.d("TAG", showCheck + "");
        isShowCheckIcon = showCheck;
    }

    @Override
    public void showSearchItem(boolean showSearch) {
        Log.d("TAG", showSearch + "");
        isShowSearchIcon = showSearch;
    }

    @Override
    public void setToolbar(Toolbar toolbar) {
        if (toolbar != null) {
            mToolbar.setVisibility(View.GONE);
            setSupportActionBar(toolbar);
            toggle = new ActionBarDrawerToggle(
                    this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        } else {
            mToolbar.setVisibility(View.VISIBLE);
            setSupportActionBar(mToolbar);
            toggle = new ActionBarDrawerToggle(
                    this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        }
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();
    }
}
