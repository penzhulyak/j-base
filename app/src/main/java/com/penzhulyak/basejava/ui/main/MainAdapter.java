package com.penzhulyak.basejava.ui.main;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListUpdateCallback;
import androidx.recyclerview.widget.RecyclerView;

import com.penzhulyak.basejava.base.BaseViewHolder;
import com.penzhulyak.basejava.data.local.db.entity.User;
import com.penzhulyak.basejava.databinding.ItemUserBinding;
import com.penzhulyak.basejava.databinding.ItemUserHeaderBinding;
import com.penzhulyak.basejava.diffutills.UserListDiffUtil;

import java.util.ArrayList;
import java.util.List;

public class MainAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;

    private List<User> mList;
    private User header;
    private ListUpdateCallback listUpdateCallback = new ListUpdateCallback() {
        @Override
        public void onInserted(int position, int count) {
            notifyItemRangeInserted(position, count);
        }

        @Override
        public void onRemoved(int position, int count) {
            notifyItemRangeRemoved(position, count);
        }

        @Override
        public void onMoved(int fromPosition, int toPosition) {
            notifyItemMoved(fromPosition, toPosition);
        }

        @Override
        public void onChanged(int position, int count, Object payload) {
            notifyItemRangeChanged(position, count);
        }
    };

    public MainAdapter() {
        mList = new ArrayList<>();
        header = new User("HEADER", null);
        mList.add(header);
    }

    public void setItems(List<User> list) {
//        if (mList != null) {
        DiffUtil.DiffResult result = DiffUtil.calculateDiff(new UserListDiffUtil(mList, list), true);
        result.dispatchUpdatesTo(listUpdateCallback);
        mList.clear();
//        }
        mList.addAll(list);
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_HEADER:
                ItemUserHeaderBinding botViewHolder = ItemUserHeaderBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new HeaderViewHolder(botViewHolder);
            case TYPE_ITEM:
            default:
                ItemUserBinding userImgListViewHolder = ItemUserBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new UserViewHolder(userImgListViewHolder);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemViewType(int position) {
        User user = mList.get(position);
        if (user.getLogin().equals("HEADER")) {
            return TYPE_HEADER;
        } else
            return TYPE_ITEM;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public class HeaderViewHolder extends BaseViewHolder {
        ItemUserHeaderBinding binding;
        private int position;

        public HeaderViewHolder(ItemUserHeaderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        public void onBind(int position) {

        }
    }

    public class UserViewHolder extends BaseViewHolder {
        ItemUserBinding binding;
        private int position;

        public UserViewHolder(ItemUserBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @Override
        public void onBind(int position) {
            this.position = position;
            User user = mList.get(position);
            binding.setUser(user);
        }
    }
}
