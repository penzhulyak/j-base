package com.penzhulyak.basejava.ui.main;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.penzhulyak.basejava.R;
import com.penzhulyak.basejava.base.BaseFragment;
import com.penzhulyak.basejava.data.local.db.entity.User;
import com.penzhulyak.basejava.databinding.FragmentMainBinding;
import com.penzhulyak.basejava.di.viewmodel.ViewModelFactory;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MainFragment extends BaseFragment {

    NavController navController;
    @Inject
    ActionBarBuilder actionBarBuilder;
    @Inject
    ViewModelFactory viewModelFactory;
    private ActionBar actionbar;
    private MainAdapter adapter;
    private MainViewModel mViewModel;
    private FragmentMainBinding binding;
    private AlertDialog dialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel.class);

        actionBarBuilder
                .setTitle("Search")
                .setToolbar(null)
                .setShowCheckItem(false)
                .setShowSearchItem(true)
                .build(getActivity());
//        mViewModel.init();
        mViewModel.isAddClicked().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean)
                    showDialog();
            }
        });
        mViewModel.getAllUsers().observe(this, users -> {
            List<User> l = new ArrayList<>();
            l.add(new User("HEADER", null));
            for (User user : users) {
                Log.d("TAG", user.toString() + "\n");
            }
            Log.d("TAG", String.valueOf(users.size()));
            l.addAll(users);
            adapter.setItems(l);
        });

        mViewModel.isNeedShowMessage().observe(this, aBoolean
                -> Toast.makeText(getActivity(), mViewModel.getMessageText().getValue(), Toast.LENGTH_SHORT).show());

        setUpRecycler();

        binding.setLifecycleOwner(this);
        binding.setViewModel(mViewModel);
    }

    private void setUpRecycler() {
        adapter = new MainAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.recyclerView.setAdapter(adapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(),
                layoutManager.getOrientation());
        binding.recyclerView.addItemDecoration(dividerItemDecoration);
        binding.recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Добавление нового пользователя");
        final View customLayout = getLayoutInflater().inflate(R.layout.dialog_add_user, null);
        EditText name = customLayout.findViewById(R.id.etName);
        name.setText(mViewModel.getDialogNameMessage().getValue());
        name.addTextChangedListener(new InputTextWatcher((charSequence, count)
                -> mViewModel.setDialogNameMessage(charSequence.toString())));
        EditText password = customLayout.findViewById(R.id.etPassword);
        password.setText(mViewModel.getDialogPasswordMessage().getValue());
        password.addTextChangedListener(new InputTextWatcher((charSequence, count)
                -> mViewModel.setDialogPasswordMessage(charSequence.toString())));
        builder.setView(customLayout);
        builder.setPositiveButton("Добавить", (dialogInterface, i) -> {
            mViewModel.createNewUser(name.getText().toString(), password.getText().toString());
            mViewModel.setDialogNameMessage("");
            mViewModel.setDialogPasswordMessage("");
        });
        dialog = builder.create();
        dialog.setOnCancelListener(dialogInterface -> mViewModel.setAddClicked(false));
        dialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (dialog != null)
            dialog.dismiss();
    }

    public interface InputTextWatcherCalback {
        void onTextChanged(CharSequence charSequence, int count);
    }

    public class InputTextWatcher implements TextWatcher {

        InputTextWatcherCalback watcher;

        public InputTextWatcher(InputTextWatcherCalback watcher) {
            this.watcher = watcher;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            watcher.onTextChanged(charSequence, i2);

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }
}