package com.penzhulyak.basejava.ui.categories;

import com.penzhulyak.basejava.base.BaseViewModel;
import com.penzhulyak.basejava.data.AppRepository;
import com.penzhulyak.basejava.interactors.SearchInteractor;

import javax.inject.Inject;

public class CategoriesViewModel extends BaseViewModel {
    private AppRepository mRepository;

    @Inject
    public CategoriesViewModel(AppRepository mAppRepository, SearchInteractor mSearchInteractor) {
        super(mSearchInteractor);
        mRepository = mAppRepository;
    }
}