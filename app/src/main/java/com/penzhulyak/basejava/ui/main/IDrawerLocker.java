package com.penzhulyak.basejava.ui.main;

public interface IDrawerLocker {
    void setDrawerEnabled(boolean enabled);
}
