package com.penzhulyak.basejava.ui.main;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.penzhulyak.basejava.base.BaseViewModel;
import com.penzhulyak.basejava.data.AppRepository;
import com.penzhulyak.basejava.data.local.db.entity.Filter;
import com.penzhulyak.basejava.data.local.db.entity.User;
import com.penzhulyak.basejava.interactors.SearchInteractor;
import com.penzhulyak.basejava.utils.SingleLiveEvent;

import java.util.List;

import javax.inject.Inject;

public class MainViewModel extends BaseViewModel {
    private final MutableLiveData<Boolean> isAddClicked = new MutableLiveData<>();
    //    private final MutableLiveData<String> searchField = new MutableLiveData<>();
    private final MutableLiveData<Filter> filter = new MutableLiveData<>();
    private final MutableLiveData<String> messageText = new MutableLiveData<>();
    private final MutableLiveData<String> dialogNameMessage = new MutableLiveData<>();
    private final MutableLiveData<String> dialogPasswordMessage = new MutableLiveData<>();
    private final SingleLiveEvent<Boolean> isNeedShowMessage = new SingleLiveEvent<>();
    private final MutableLiveData<Boolean> onSearchItemClicked = new MutableLiveData<>();
    Filter f = new Filter();
    private AppRepository mRepository;
    private LiveData<List<User>> allUsers;

    @Inject
    public MainViewModel(AppRepository mAppRepository, SearchInteractor mSearchInteractor) {
        super(mSearchInteractor);
        mRepository = mAppRepository;

//        searchField.setValue("");

//        allUsers = Transformations.switchMap(filter, new Function<Filter, LiveData<List<User>>>() {
//            @Override
//            public LiveData<List<User>> apply(Filter input) {
//                return mRepository.getAny(input.getFrom(), input.getTo());
//            }
//        });
        init();
    }

    void init() {
        setDialogNameMessage("");
        setDialogPasswordMessage("");
        setSearchField("");
        setFilter(0, 500);
        allUsers = Transformations.switchMap(getSearchField(), new Function<String, LiveData<List<User>>>() {
            @Override
            public LiveData<List<User>> apply(String input) {
                if (input.length() > 0)
                    return mRepository.getFilteredUsers(input);
                else
                    return mRepository.getAllUsers();
            }
        });
    }

    public MutableLiveData<Boolean> getOnSearchItemClicked() {
        return onSearchItemClicked;
    }

    public void setOnSearchItemClicked(boolean value) {
        onSearchItemClicked.setValue(value);
    }

    public MutableLiveData<Boolean> isAddClicked() {
        return isAddClicked;
    }

    public void setFilter(long from, long to) {
        f.setTo(to);
        f.setFrom(from);
        filter.setValue(f);
    }

    public void setAddClicked(boolean value) {
        isAddClicked.setValue(value);
    }

//    public MutableLiveData<String> getSearchField() {
//        return searchField;
//    }
//
//    public void setSearchField(String value) {
//        searchField.setValue(value);
//    }

    public MutableLiveData<String> getDialogNameMessage() {
        return dialogNameMessage;
    }

    public void setDialogNameMessage(String value) {
        dialogNameMessage.setValue(value);
    }

    public MutableLiveData<String> getDialogPasswordMessage() {
        return dialogPasswordMessage;
    }

    public void setDialogPasswordMessage(String value) {
        dialogPasswordMessage.setValue(value);
    }

    public void createNewUser(String name, String password) {
        mRepository.insertUser(new User(name, password));
    }

    public LiveData<List<User>> getAllUsers() {
        return allUsers;
    }

    public MutableLiveData<String> getMessageText() {
        return messageText;
    }

    public void setMessageText(String value) {
        messageText.setValue(value);
    }

    public SingleLiveEvent<Boolean> isNeedShowMessage() {
        return isNeedShowMessage;
    }

    public void setNeedShowMessage(boolean value) {
        isNeedShowMessage.setValue(value);
    }
}