package com.penzhulyak.basejava.ui.settings;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;

import com.penzhulyak.basejava.R;
import com.penzhulyak.basejava.base.BaseFragment;
import com.penzhulyak.basejava.databinding.FragmentSettingsBinding;
import com.penzhulyak.basejava.di.viewmodel.ViewModelFactory;
import com.penzhulyak.basejava.ui.main.ActionBarBuilder;
import com.penzhulyak.basejava.ui.main.MainAdapter;

import javax.inject.Inject;

public class SettingsFragment extends BaseFragment {

    NavController navController;
    @Inject
    ActionBarBuilder actionBarBuilder;
    @Inject
    ViewModelFactory viewModelFactory;
    private ActionBar actionbar;
    private MainAdapter adapter;
    private SettingsViewModel mViewModel;
    private FragmentSettingsBinding binding;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(SettingsViewModel.class);

        actionBarBuilder
                .setTitle("Settings")
                .setToolbar(null)
                .setShowCheckItem(true)
                .setShowSearchItem(false)
                .build(getActivity());

        mViewModel.init();

        binding.setLifecycleOwner(this);
        binding.setViewModel(mViewModel);
    }
}