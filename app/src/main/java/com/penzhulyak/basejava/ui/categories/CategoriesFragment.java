package com.penzhulyak.basejava.ui.categories;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;

import com.penzhulyak.basejava.R;
import com.penzhulyak.basejava.base.BaseFragment;
import com.penzhulyak.basejava.databinding.FragmentCategoriesBinding;
import com.penzhulyak.basejava.di.viewmodel.ViewModelFactory;
import com.penzhulyak.basejava.ui.main.ActionBarBuilder;

import javax.inject.Inject;

public class CategoriesFragment extends BaseFragment {
    NavController navController;
    @Inject
    ActionBarBuilder actionBarBuilder;
    @Inject
    ViewModelFactory viewModelFactory;
    private ActionBar actionbar;
    private CategoriesViewModel mViewModel;
    private FragmentCategoriesBinding binding;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_categories, container, false);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(CategoriesViewModel.class);

        actionBarBuilder
                .setTitle("Categories")
                .setToolbar(null)
                .setShowCheckItem(false)
                .setShowSearchItem(true)
                .build(getActivity());

        binding.setLifecycleOwner(this);
        binding.setViewModel(mViewModel);
    }
}