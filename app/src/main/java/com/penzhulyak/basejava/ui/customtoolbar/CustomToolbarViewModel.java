package com.penzhulyak.basejava.ui.customtoolbar;

import androidx.lifecycle.MutableLiveData;

import com.penzhulyak.basejava.base.BaseViewModel;
import com.penzhulyak.basejava.data.AppRepository;
import com.penzhulyak.basejava.interactors.SearchInteractor;

import javax.inject.Inject;

public class CustomToolbarViewModel extends BaseViewModel {
    private final MutableLiveData<String> toolbarTitle = new MutableLiveData<>();
    private final MutableLiveData<Boolean> isTitleCardVisible = new MutableLiveData<>();

    private AppRepository mRepository;

    @Inject
    public CustomToolbarViewModel(AppRepository mAppRepository, SearchInteractor mSearchInteractor) {
        super(mSearchInteractor);
        mRepository = mAppRepository;
        setCardTitleVisible(true);
        setToolbarTitle("Fab");
    }

    public MutableLiveData<String> getToolbarTitle() {
        return toolbarTitle;
    }

    public void setToolbarTitle(String value) {
        toolbarTitle.setValue(value);
    }

    public MutableLiveData<Boolean> isTitleCardVisible() {
        return isTitleCardVisible;
    }

    public void setCardTitleVisible(boolean value) {
        isTitleCardVisible.setValue(value);
    }
}