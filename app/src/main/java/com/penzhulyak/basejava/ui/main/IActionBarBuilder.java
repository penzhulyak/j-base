package com.penzhulyak.basejava.ui.main;

import androidx.appcompat.widget.Toolbar;

public interface IActionBarBuilder {

    void setToolbarTitle(CharSequence title);

    void setToolbarVisible(boolean visible);

    void showCheckItem(boolean showCheck);

    void showSearchItem(boolean showSearch);

    void setToolbar(Toolbar toolbar);
}
