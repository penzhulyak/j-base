package com.penzhulyak.basejava.ui.customtoolbar;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.cardview.widget.CardView;
import androidx.core.view.ViewCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;

import com.google.android.material.appbar.AppBarLayout;
import com.penzhulyak.basejava.R;
import com.penzhulyak.basejava.base.BaseFragment;
import com.penzhulyak.basejava.databinding.FragmentCustomToolbarBinding;
import com.penzhulyak.basejava.di.viewmodel.ViewModelFactory;
import com.penzhulyak.basejava.ui.main.ActionBarBuilder;

import javax.inject.Inject;

public class CustomToolbarFragment extends BaseFragment implements AppBarLayout.OnOffsetChangedListener {
    private static final int PERCENTAGE_TO_SHOW_FAB = 50;
    private static final int PERCENTAGE_TO_SHOW_CARDVIEW = 100;
    NavController navController;
    @Inject
    ActionBarBuilder actionBarBuilder;
    @Inject
    ViewModelFactory viewModelFactory;
    private ActionBar actionbar;
    private CustomToolbarViewModel mViewModel;
    private FragmentCustomToolbarBinding binding;
    private View mFab;
    private CardView mAnchoredCardView;
    private int mMaxScrollSize;
    private boolean mIsFabHidden;
    private boolean mIsCardViewHidden;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_custom_toolbar, container, false);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(CustomToolbarViewModel.class);
        mFab = binding.fab;
        mAnchoredCardView = binding.anchoredCardView;
        AppBarLayout appbar = binding.appBar;
        appbar.addOnOffsetChangedListener(this);

        actionBarBuilder
                .setShowCheckItem(false)
                .setShowSearchItem(true)
                .setToolbar(binding.toolbar)
                .build(getActivity());

        binding.setLifecycleOwner(this);
        binding.setViewModel(mViewModel);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();

        int currentScrollPercentage = (Math.abs(i)) * 100
                / mMaxScrollSize;

        if (currentScrollPercentage >= PERCENTAGE_TO_SHOW_FAB) {
            if (!mIsFabHidden) {
                mIsFabHidden = true;
                ViewCompat.animate(mFab).scaleY(0).scaleX(0).start();
            }
        }
        if (currentScrollPercentage < PERCENTAGE_TO_SHOW_FAB) {
            if (mIsFabHidden) {
                mIsFabHidden = false;
                ViewCompat.animate(mFab).scaleY(1).scaleX(1).start();
            }
        }

        if (currentScrollPercentage >= PERCENTAGE_TO_SHOW_CARDVIEW) {
            if (!mIsCardViewHidden) {
                mIsCardViewHidden = true;
                mViewModel.setCardTitleVisible(false);
//                animateView(false, mAnchoredCardView);
            }
        }

        if (currentScrollPercentage < PERCENTAGE_TO_SHOW_CARDVIEW) {
            if (mIsCardViewHidden) {
                mIsCardViewHidden = false;
                mViewModel.setCardTitleVisible(true);
//                animateView(true, mAnchoredCardView);
            }
        }
    }


}