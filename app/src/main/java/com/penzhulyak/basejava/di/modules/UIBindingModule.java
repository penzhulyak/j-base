package com.penzhulyak.basejava.di.modules;

import com.penzhulyak.basejava.di.scope.ActivityScope;
import com.penzhulyak.basejava.di.scope.FragmentScope;
import com.penzhulyak.basejava.ui.categories.CategoriesFragment;
import com.penzhulyak.basejava.ui.customtoolbar.CustomToolbarFragment;
import com.penzhulyak.basejava.ui.main.MainActivity;
import com.penzhulyak.basejava.ui.main.MainFragment;
import com.penzhulyak.basejava.ui.settings.SettingsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class UIBindingModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract MainActivity bindMainActivity();

    @FragmentScope
    @ContributesAndroidInjector
    abstract MainFragment bindMainFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract CategoriesFragment bindCategoriesFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract SettingsFragment bindSettingsFragment();

    @FragmentScope
    @ContributesAndroidInjector
    abstract CustomToolbarFragment bindCustomToolbarFragment();
}
