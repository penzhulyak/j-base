package com.penzhulyak.basejava.di.modules;

import android.app.Application;
import android.content.Context;

import com.penzhulyak.basejava.interactors.SearchInteractor;
import com.penzhulyak.basejava.ui.main.ActionBarBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }

    @Provides
    @Singleton
    ActionBarBuilder provideActionBar() {
        return new ActionBarBuilder();
    }

    @Provides
    @Singleton
    SearchInteractor provideSearchInteractor() {
        return new SearchInteractor();
    }
}
