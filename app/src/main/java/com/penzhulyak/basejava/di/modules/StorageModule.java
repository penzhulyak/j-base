package com.penzhulyak.basejava.di.modules;

import android.app.Application;

import androidx.room.Room;

import com.penzhulyak.basejava.data.AppRepository;
import com.penzhulyak.basejava.data.IDataSourceManager;
import com.penzhulyak.basejava.data.local.db.AppDatabase;
import com.penzhulyak.basejava.data.local.db.DataBaseHelper;
import com.penzhulyak.basejava.data.local.db.IDatabaseHelper;
import com.penzhulyak.basejava.data.local.db.dao.UserDao;
import com.penzhulyak.basejava.data.local.prefs.IPreferencesHelper;
import com.penzhulyak.basejava.data.local.prefs.PreferencesHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class StorageModule {
    @Provides
    @Singleton
    static IDataSourceManager provideDataManager(AppRepository repository) {
        return repository;
    }

    @Provides
    @Singleton
    static AppDatabase provideDb(Application context) {
        return Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "app.db")
                .fallbackToDestructiveMigration()
                .build();
    }

    @Provides
    @Singleton
    static IDatabaseHelper provideDatabaseHelper(DataBaseHelper dataBaseHelper) {
        return dataBaseHelper;
    }

    @Provides
    @Singleton
    static UserDao provideTasksDao(AppDatabase db) {
        return db.userDao();
    }

    @Provides
    @Singleton
    static IPreferencesHelper providePreferencesHelper(PreferencesHelper preferencesHelper) {
        return preferencesHelper;
    }

    @Provides
    @Singleton
    static String providePreferenceName() {
        return "app_pref";
    }

}
