package com.penzhulyak.basejava.di.viewmodel;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.penzhulyak.basejava.ui.categories.CategoriesViewModel;
import com.penzhulyak.basejava.ui.customtoolbar.CustomToolbarViewModel;
import com.penzhulyak.basejava.ui.main.MainViewModel;
import com.penzhulyak.basejava.ui.settings.SettingsViewModel;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Singleton
    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);

    @Binds
    @IntoMap
    @Singleton
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindMainViewModel(MainViewModel mainViewModel);

    @Binds
    @IntoMap
    @Singleton
    @ViewModelKey(SettingsViewModel.class)
    abstract ViewModel bindSettingsViewModel(SettingsViewModel settingsViewModel);

    @Binds
    @IntoMap
    @Singleton
    @ViewModelKey(CategoriesViewModel.class)
    abstract ViewModel bindCategoriesViewModel(CategoriesViewModel categoriesViewModel);

    @Binds
    @IntoMap
    @Singleton
    @ViewModelKey(CustomToolbarViewModel.class)
    abstract ViewModel bindCustomToolbarViewModel(CustomToolbarViewModel customToolbarViewModel);
}
